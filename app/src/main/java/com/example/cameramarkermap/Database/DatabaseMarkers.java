package com.example.cameramarkermap.Database;

import androidx.annotation.NonNull;

import com.example.cameramarkermap.Model.MyMarker;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

public class DatabaseMarkers
{
    public static final String DATABASE_REFERENCE = "Markers";
    public static final String STORAGE_REFERENCE = "Images Markers";

    public DatabaseReference databaseReference;
    public StorageReference storageReference;

    public void initialize()
    {
        databaseReference = FirebaseDatabase.getInstance().getReference(DATABASE_REFERENCE);
        storageReference = FirebaseStorage.getInstance().getReference().child(STORAGE_REFERENCE);
    }

    public String updateDatabase(MyMarker myMarker)
    {
        String id = myMarker.getId();
        if(id == null || id.isEmpty())
        {
            id = databaseReference.push().getKey();
            myMarker.setId(id);
        }
        assert id != null;
        databaseReference.child(id).setValue(myMarker);
        return id;
    }

    public void remove(String id) { databaseReference.child(id).removeValue(); }

    public void useMarker(String comic_id, GetDataListener<MyMarker> listener)
    {
        databaseReference.child(comic_id).addListenerForSingleValueEvent(new ValueEventListener()
        {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) { listener.onSuccess(dataSnapshot.getValue(MyMarker.class)); databaseReference.removeEventListener(this);}

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) { }
        });
    }

    public interface GetDataListener<T> { void onSuccess(T value);}
}
