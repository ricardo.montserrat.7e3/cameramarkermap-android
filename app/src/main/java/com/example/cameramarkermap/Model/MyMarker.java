package com.example.cameramarkermap.Model;

import com.google.android.gms.maps.model.LatLng;

import java.io.Serializable;

public class MyMarker implements Serializable
{
    private String id, title, snippet;
    private double latitude, longitude;

    private String imageUrl = "";

    public MyMarker() { }

    public MyMarker(String id, String title, String snippet, LatLng position)
    {
        this.id = id;
        this.title = title;
        this.snippet = snippet;
        latitude = position.latitude;
        longitude = position.longitude;
    }

    public MyMarker(LatLng position)
    {
        latitude = position.latitude;
        longitude = position.longitude;
    }

    public String getTitle() { return title; }

    public void setTitle(String title) { this.title = title; }

    public String getSnippet() { return snippet; }

    public void setSnippet(String snippet) { this.snippet = snippet; }

    public String getId() { return id; }

    public void setId(String id) { this.id = id; }

    public LatLng obtainLatLng() { return new LatLng(latitude, longitude); }

    public void changeLatLng(LatLng latLng)
    {
        longitude = latLng.longitude;
        latitude = latLng.latitude;
    }

    public String getImageUrl() { return imageUrl; }

    public void setImageUrl(String imageUrl) { this.imageUrl = imageUrl; }

    public double getLatitude() { return latitude; }

    public void setLatitude(double latitude) { this.latitude = latitude; }

    public double getLongitude() { return longitude; }

    public void setLongitude(double longitude) { this.longitude = longitude; }
}
