package com.example.cameramarkermap.Adapters;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

public class TabAdapter extends FragmentPagerAdapter
{
    private final List<Fragment> m_FragmentList = new ArrayList<>();
    private final List<String> m_FragmentTitleList = new ArrayList<>();

    public TabAdapter(@NonNull FragmentManager fm) { super(fm); }

    public void addFragment(Fragment fragment, String title)
    {
        m_FragmentList.add(fragment);
        m_FragmentTitleList.add(title);
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return m_FragmentTitleList.get(position);
    }

    @NonNull
    @Override
    public Fragment getItem(int position)
    {
        return m_FragmentList.get(position);
    }

    @Override
    public int getCount() { return m_FragmentList.size(); }
}
