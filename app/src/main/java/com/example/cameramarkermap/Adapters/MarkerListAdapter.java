package com.example.cameramarkermap.Adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.cameramarkermap.Database.DatabaseMarkers;
import com.example.cameramarkermap.MainActivity;
import com.example.cameramarkermap.Model.MyMarker;
import com.example.cameramarkermap.R;
import com.example.cameramarkermap.Utilities.Debug;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;

public class MarkerListAdapter extends FirebaseRecyclerAdapter<MyMarker, MarkerListAdapter.MarkerListViewHolder>
{
    private DatabaseMarkers.GetDataListener<MyMarker> onMarkerItemClick, onMarkerImageClick;

    public MarkerListAdapter(@NonNull FirebaseRecyclerOptions<MyMarker> options) { super(options); }

    @NonNull
    @Override
    public MarkerListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) { return new MarkerListViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_marker_item, parent ,false)); }

    @Override
    protected void onBindViewHolder(@NonNull MarkerListViewHolder holder, int position, @NonNull MyMarker model)
    {
        holder.titleTextView.setText(model.getTitle());
        holder.snippetTextView.setText(model.getSnippet());
        MainActivity.instance.cameraManager.loadInto(holder.imageView, model.getImageUrl());

        holder.itemView.setOnClickListener(x -> onMarkerItemClick.onSuccess(model));
        holder.imageView.setOnClickListener(x -> onMarkerImageClick.onSuccess(model));
    }

    public void setOnMarkerItemClick(DatabaseMarkers.GetDataListener<MyMarker> onMarkerItemClick) { this.onMarkerItemClick = onMarkerItemClick; }

    public void setOnMarkerImageClick(DatabaseMarkers.GetDataListener<MyMarker> onMarkerImageClick) { this.onMarkerImageClick = onMarkerImageClick; }

    public static class MarkerListViewHolder extends RecyclerView.ViewHolder
    {
        private final TextView titleTextView;
        private final TextView snippetTextView;
        private final ImageView imageView;

        public MarkerListViewHolder(@NonNull View itemView)
        {
            super(itemView);

            titleTextView = itemView.findViewById(R.id.title_text_view);
            snippetTextView = itemView.findViewById(R.id.snippet_text_view);
            imageView = itemView.findViewById(R.id.image_view);
        }
    }
}
