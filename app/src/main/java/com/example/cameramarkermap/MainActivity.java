package com.example.cameramarkermap;

import android.content.Intent;
import android.graphics.Camera;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.example.cameramarkermap.Adapters.TabAdapter;
import com.example.cameramarkermap.Database.DatabaseMarkers;
import com.example.cameramarkermap.Fragments.MapFragment;
import com.example.cameramarkermap.Fragments.MarkerCreator;
import com.example.cameramarkermap.Fragments.MarkersListFragment;
import com.example.cameramarkermap.Utilities.CameraManager;
import com.google.android.material.tabs.TabLayout;

public class MainActivity extends AppCompatActivity
{
    public static MainActivity instance;

    public DatabaseMarkers databaseMarkers = new DatabaseMarkers();
    public CameraManager cameraManager;

    public ViewPager viewPager;

    private Fragment currentFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        instance = this;

        viewPager = findViewById(R.id.view_pager);

        databaseMarkers.initialize();

        cameraManager = new CameraManager(MainActivity.this, databaseMarkers.storageReference);

        TabAdapter adapter = new TabAdapter(getSupportFragmentManager());

        adapter.addFragment(new MarkersListFragment(), "Markers");
        adapter.addFragment(new MapFragment(), "Map");

        viewPager.setAdapter(adapter);

        ((TabLayout)findViewById(R.id.tabs_layout)).setupWithViewPager(viewPager);
    }

    public void showTab() { showTab(true); }

    private void showTab(boolean show) { viewPager.setVisibility(show? View.VISIBLE : View.GONE); }

    public void hideTab() { showTab(false); }

    public void goToFragment(Fragment fragment) { getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, currentFragment = fragment).commit(); }

    public void removeCurrentFragment() { getSupportFragmentManager().beginTransaction().remove(currentFragment).commit(); }

    @Override
    public void onBackPressed()
    {
        if(currentFragment != null && currentFragment instanceof MarkerCreator) ((MarkerCreator) currentFragment).closeCreator();
        else super.onBackPressed();
    }
}