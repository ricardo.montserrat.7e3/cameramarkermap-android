package com.example.cameramarkermap.Fragments;

import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.cameramarkermap.Adapters.MarkerListAdapter;
import com.example.cameramarkermap.Adapters.SwipeToDeleteCallback;
import com.example.cameramarkermap.MainActivity;
import com.example.cameramarkermap.Model.MyMarker;
import com.example.cameramarkermap.R;
import com.firebase.ui.database.FirebaseRecyclerOptions;

public class MarkersListFragment extends Fragment
{
    public static MarkerListAdapter adapter;

    public MarkersListFragment() { }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View v = inflater.inflate(R.layout.fragment_markers_list, container, false);
        RecyclerView recyclerView = v.findViewById(R.id.list_markers);

        FirebaseRecyclerOptions<MyMarker> options = new FirebaseRecyclerOptions
                .Builder<MyMarker>()
                .setQuery(MainActivity.instance.databaseMarkers.databaseReference, MyMarker.class).build();

        recyclerView.setAdapter(adapter = new MarkerListAdapter(options));
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        new ItemTouchHelper(new SwipeToDeleteCallback(holder -> MainActivity.instance.databaseMarkers.remove(adapter.getItem(holder.getAdapterPosition()).getId()))).attachToRecyclerView(recyclerView);
        return v;
    }

    @Override
    public void onStart()
    {
        super.onStart();
        adapter.startListening();
    }

    @Override
    public void onStop()
    {
        super.onStop();
        adapter.stopListening();
    }
}