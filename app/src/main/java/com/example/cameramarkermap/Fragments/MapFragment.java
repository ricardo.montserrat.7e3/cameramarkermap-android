package com.example.cameramarkermap.Fragments;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import com.example.cameramarkermap.MainActivity;
import com.example.cameramarkermap.Model.MyMarker;
import com.example.cameramarkermap.R;
import com.example.cameramarkermap.Utilities.ConvertURLToBitmap;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.util.Objects;

public class MapFragment extends Fragment implements OnMapReadyCallback
{
    public static final int LOCATION_PERMISSION_REQUEST_CODE = 1;
    public static final LatLng DEFAULT_MISSING_LOCATION = new LatLng(41.38879, 2.15899);

    private GoogleMap gMap;

    private LatLng lastPos;

    private ValueEventListener valueEventListener;

    public MapFragment() { }

    //region INITIALIZATION
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) { return inflater.inflate(R.layout.fragment_map, container, false); }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);

        MapView mapView = view.findViewById(R.id.map_view);

        checkLocationPermissions();

        if (mapView != null)
        {
            mapView.onCreate(null);
            mapView.onResume();
            mapView.getMapAsync(this);
        }

        view.findViewById(R.id.add_marker_button).setOnClickListener(x ->
        {
            checkLocationPermissions();
            enableLocation();

            goToMarkerCreator(new MyMarker(getLastLocation()));
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap)
    {
        gMap = googleMap;

        enableLocation();

        gMap.setMinZoomPreference(15);
        gMap.setMaxZoomPreference(18);

        gMap.animateCamera(getCameraMovement(getLastLocation(), 15, 0, 30));

        gMap.setOnMapLongClickListener(x ->
        {
            MyMarker myMarker = new MyMarker("", "New Marker", "Add Description Here!", new LatLng(x.latitude, x.longitude));
            MainActivity.instance.databaseMarkers.updateDatabase(myMarker);
            addMarker(myMarker);
        });

        MarkersListFragment.adapter.setOnMarkerItemClick(this::goToMarkerCreator);

        MarkersListFragment.adapter.setOnMarkerImageClick(marker ->
        {
            MainActivity.instance.viewPager.setCurrentItem(1);
            gMap.animateCamera(getCameraMovement(marker.obtainLatLng(), 15, 0, 25));
        });

        valueEventListener = new ValueEventListener()
        {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) { gMap.clear(); snapshot.getChildren().forEach(data -> addMarker(Objects.requireNonNull(data.getValue(MyMarker.class)))); }

            @Override
            public void onCancelled(@NonNull DatabaseError error) { }
        };

        MainActivity.instance.databaseMarkers.databaseReference.addValueEventListener(valueEventListener);
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        MainActivity.instance.databaseMarkers.databaseReference.removeEventListener(valueEventListener);
    }

    //endregion

    //region MAP_METHODS
    private void goToMarkerCreator(MyMarker myMarker)
    {
        Bundle bundle = new Bundle();
        bundle.putSerializable("marker", myMarker);

        MarkerCreator markerCreator = new MarkerCreator();
        markerCreator.setArguments(bundle);

        MainActivity.instance.hideTab();
        MainActivity.instance.goToFragment(markerCreator);
    }

    @SuppressLint("MissingPermission")
    private LatLng getLastLocation()
    {
        if (checkLocationPermissions())
        {
            FusedLocationProviderClient fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(Objects.requireNonNull(getContext()));
            fusedLocationProviderClient.getLastLocation()
                    .addOnSuccessListener(Objects.requireNonNull(getActivity()), location ->
                    {
                        if (lastPos != null)
                            lastPos = new LatLng(location.getLatitude(), location.getLongitude());
                    });
        }
        return lastPos != null ? lastPos : DEFAULT_MISSING_LOCATION;
    }

    private void addMarker(MyMarker marker) { addMarker(marker.obtainLatLng(), marker.getTitle(), marker.getSnippet(), marker.getId(), marker.getImageUrl()); }

    private void addMarker(LatLng coordinates, String name, String snippet, String id, String imageUrl)
    {
        MarkerOptions marker = new MarkerOptions();

        marker.position(coordinates);
        marker.title(name);
        marker.snippet(snippet);
        marker.draggable(true);
        marker.icon(imageUrl.isEmpty()? BitmapDescriptorFactory.fromResource(android.R.drawable.ic_menu_info_details) : BitmapDescriptorFactory.fromBitmap(ConvertURLToBitmap.getBitmapFromURL(imageUrl)));

        gMap.addMarker(marker).setTag(id);
    }

    private CameraUpdate getCameraMovement(LatLng location, int zoom, int bearing, int tilt)
    {
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(location)
                .zoom(zoom)
                .bearing(bearing)
                .tilt(tilt)
                .build();
        return CameraUpdateFactory.newCameraPosition(cameraPosition);
    }
    //endregion

    //region PERMISSIONS_MANAGEMENT

    private boolean checkLocationPermissions()
    {
        if (needsPermission(Manifest.permission.ACCESS_FINE_LOCATION) || needsPermission(Manifest.permission.ACCESS_COARSE_LOCATION))
        {
            requestNecessaryPermissions();
            return false;
        }
        return true;
    }

    private boolean needsPermission(String permission) { return ActivityCompat.checkSelfPermission(Objects.requireNonNull(getContext()), permission) != PackageManager.PERMISSION_GRANTED; }

    @SuppressLint("MissingPermission")
    private void enableLocation() { gMap.setMyLocationEnabled(checkLocationPermissions()); }

    private void requestNecessaryPermissions()
    {
        ActivityCompat.requestPermissions(Objects.requireNonNull(getActivity()),
                new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, LOCATION_PERMISSION_REQUEST_CODE);
    }

    //endregion
}