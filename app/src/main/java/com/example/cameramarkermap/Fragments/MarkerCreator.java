package com.example.cameramarkermap.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.cameramarkermap.Database.DatabaseMarkers;
import com.example.cameramarkermap.MainActivity;
import com.example.cameramarkermap.Model.MyMarker;
import com.example.cameramarkermap.R;
import com.theartofdev.edmodo.cropper.CropImage;

import java.io.File;
import java.util.Objects;

import static android.app.Activity.RESULT_OK;

public class MarkerCreator extends Fragment
{
    private EditText markerTitleEdit, markerSnippetEdit;
    private ImageView markerImage;

    private MyMarker currentMarker;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        View v = inflater.inflate(R.layout.activity_marker_creator, container, false);

        Bundle values = getArguments();
        currentMarker = values != null? (MyMarker) values.getSerializable("marker") : new MyMarker(MapFragment.DEFAULT_MISSING_LOCATION);

        markerTitleEdit = v.findViewById(R.id.marker_title_edit);
        markerSnippetEdit = v.findViewById(R.id.marker_snippet_edit);
        markerImage = v.findViewById(R.id.marker_image_view);

        markerTitleEdit.setText(currentMarker.getTitle());
        markerSnippetEdit.setText(currentMarker.getSnippet());
        MainActivity.instance.cameraManager.loadInto(markerImage, currentMarker.getImageUrl());

        v.findViewById(R.id.edit_add_marker_button).setOnClickListener(x ->
        {
            currentMarker.setTitle(markerTitleEdit.getText().toString());
            currentMarker.setSnippet(markerSnippetEdit.getText().toString());

            if(currentMarker.getTitle().isEmpty() || currentMarker.getSnippet().isEmpty()) Toast.makeText(getContext(), "Please, fill completely the required marker values!", Toast.LENGTH_SHORT).show();
            else
            {
                if(MainActivity.instance.cameraManager.url == null) MainActivity.instance.databaseMarkers.updateDatabase(currentMarker);
                else
                {
                    MainActivity.instance.cameraManager.compressImage();
                    MainActivity.instance.cameraManager.uploadImage(currentMarker.obtainLatLng(), url ->
                    {
                        currentMarker.setImageUrl(url);
                        MainActivity.instance.databaseMarkers.updateDatabase(currentMarker);
                    });
                }
                closeCreator();
            }
        });

        v.findViewById(R.id.button_exit_marker_creator).setOnClickListener(x -> closeCreator());

        v.findViewById(R.id.button_select_image).setOnClickListener(x -> CropImage.startPickImageActivity(Objects.requireNonNull(getContext()), this));

        return v;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode != RESULT_OK) return;

        if(requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE)
        {
            MainActivity.instance.cameraManager.cropImage(CropImage.getPickImageResultUri(Objects.requireNonNull(getContext()), data), this);
            MainActivity.instance.cameraManager.loadInto(markerImage, MainActivity.instance.cameraManager.url.toString());
        }
        else if(requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE)
        {
            MainActivity.instance.cameraManager.url = new File(Objects.requireNonNull(CropImage.getActivityResult(data)).getUri().getPath());
            MainActivity.instance.cameraManager.loadInto(markerImage, MainActivity.instance.cameraManager.url.toString());
        }
    }

    public void closeCreator()
    {
        MainActivity.instance.showTab();
        MainActivity.instance.removeCurrentFragment();
        MainActivity.instance.viewPager.setCurrentItem(0);
    }
}