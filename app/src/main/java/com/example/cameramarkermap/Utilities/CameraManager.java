package com.example.cameramarkermap.Utilities;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.widget.ImageView;

import androidx.fragment.app.Fragment;

import com.example.cameramarkermap.Database.DatabaseMarkers;
import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.storage.StorageMetadata;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;

import id.zelory.compressor.Compressor;

public class CameraManager
{
    private final Context context;
    private final StorageReference storageReference;
    public File url;

    private Bitmap thumb_bitmap;
    private byte[] thumb_byte;

    public CameraManager(Context context, StorageReference storageReference)
    {
        this.context = context;
        this.storageReference = storageReference;
    }

    public void cropImage(Uri imageUri, Fragment fragment)
    {
        CropImage.activity(imageUri).setGuidelines(CropImageView.Guidelines.ON)
                .setRequestedSize(640, 480)
                .setAspectRatio(2, 1).start(context, fragment);
    }

    public void compressImage()
    {
        try
        {
            thumb_bitmap = new Compressor(context)
                    .setMaxHeight(125)
                    .setMaxWidth(125)
                    .setQuality(50)
                    .compressToBitmap(url);
        }
        catch (Exception e) { e.printStackTrace(); }

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        thumb_bitmap.compress(Bitmap.CompressFormat.JPEG, 90, byteArrayOutputStream);
        thumb_byte = byteArrayOutputStream.toByteArray();
    }

    public void loadInto(ImageView imageView, String url) { if(!url.isEmpty()) Picasso.with(context).load(url).into(imageView); }

    public void uploadImage(LatLng latLng, DatabaseMarkers.GetDataListener<String> onUriTaskCompleted)
    {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.FRANCE);

        final StorageReference ref = storageReference.child(sdf.format(new Date()) + ".jpg");

        StorageMetadata metadata = new StorageMetadata.Builder()
                .setCustomMetadata("clave1", "Lat: " + latLng.latitude)
                .setCustomMetadata("clave2", "Lon: " + latLng.longitude)
                .build();

        ref.putBytes(thumb_byte, metadata).continueWithTask(task ->
        {
            if (!task.isSuccessful()) throw Objects.requireNonNull(task.getException());
            return ref.getDownloadUrl();
        }).addOnCompleteListener(task ->
        {
            onUriTaskCompleted.onSuccess(task.getResult().toString());
            url = null;
        });
    }
}
